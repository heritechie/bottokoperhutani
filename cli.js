require('dotenv').config();
const prompts = require('prompts');
const figlet = require('figlet');
 
(async () => {

  const figletTitle = await new Promise(function(resolve, reject) {
    figlet('WebBot\ntokoperhutani.com', function(err, data) {
      if(err) reject(err);
      resolve(data);
    });
  }) ;

  let exit = false;

  console.log(figletTitle, '\n');
  console.log('crafted by: heritechie@gmail.com');
  console.log('enjoy it!\n');

  let email = process.env.EMAIL;
  let password = process.env.PASSWORD;

  if (!email && !password) {
    const loginArgs = await prompts([
      {
        type: 'text',
        name: 'email',
        message: 'email'
      },
      {
        type: 'password',
        name: 'password',
        message: 'password'
      }
    ]);

    email = loginArgs.email.toLowerCase().trim();
    password = loginArgs.password.trim();
  }
  

  const bot = require('./src/index.js')(email, password);
 
  const loggedIn = await bot.login();

  if ('status' in loggedIn && !loggedIn.status) {
    return ;
  } else {
    while(!exit) {
      const menu = await prompts({
        type: 'select',
        name: 'selected',
        message: 'Pilih Menu',
        choices: [
          { title: 'Order', value: 'order' },
          { title: 'Check Order', value: 'check' },
          { title: 'Exit', value: 'logout'},
        ]
      });
      
      if (menu.selected === 'order') {
        const kayu = await prompts({
          type: 'autocomplete',
          name: 'selected',
          message: 'Pilih Jenis Kayu',
          choices: bot.jenis_kayu
        });
    
        const wilayah = await prompts({
          type: 'select',
          name: 'selected',
          message: 'Pilih Wilayah',
          choices: bot.wilayah
        });
    
        const kota = await prompts({
          type: 'select',
          name: 'selected',
          message: 'Pilih Kota',
          choices: bot.kota.filter((kota => kota.reff_id === wilayah.selected))
        });
      
        const tpk = await prompts({
          type: 'autocomplete',
          name: 'selected',
          message: 'Pilih TPK',
          choices: bot.tpk.filter((tpk => tpk.reff_id === kota.selected))
        });

        const mutu = await prompts({
          type: 'autocomplete',
          name: 'selected',
          message: 'Pilih Mutu',
          choices: bot.mutu
        });

        const sortimen = await prompts({
          type: 'autocomplete',
          name: 'selected',
          message: 'Pilih Sortimen',
          choices: bot.sortimen
        });
    
        const availableOrder = await bot.searching({ 
          wilayah: wilayah.selected, 
          kota: kota.selected,
          tpk: tpk.selected,
          kayu: kayu.selected,
          mutu: mutu.selected,
          sortimen: sortimen.selected
        });

        if(availableOrder.length > 0) {
          const orderList = await prompts({
            type: 'multiselect',
            name: 'selected',
            message: 'Pilih Kapling',
            choices: availableOrder
          });
  
          await bot.order(orderList.selected);
        } else {
          console.log('Kapling tidak tersedia!, silahkan coba pencarian lain');
        } 

      } else if(menu.selected === 'logout') {
        await bot.logout();
        exit = true;
      }
    }
    console.log('Program Exit!');
    return ;
  }
})();