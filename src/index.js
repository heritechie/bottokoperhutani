'use strict';

const cheerio = require('cheerio');
const URLSearchParams = require('url').URLSearchParams;
const fs = require('fs');
const nodeFetch = require('node-fetch');
class Bot {

  constructor(email, password) {
    this._email = email;
    this._password = password;
    this._baseUrl = 'https://www.tokoperhutani.com';
    this._berandaUrl = `${this._baseUrl}/beranda`;
    this._http = require('fetch-cookie')(nodeFetch);
    this._propNames = ['jenis_kayu', 'mutu', 'sortimen'];
    
    this.jenis_kayu = [];
    this.mutu = [];
    this.sortimen = [];
    this.wilayah = [];
    this.kota = [];
    this.tpk = [];
    this.availableOrder = [];
    this.dataTrolley = [];
  }

  _setPageToken() {
    let domSelector = 'input[name="POTPtokenCode"]';
    this._pageToken = this.$(domSelector).val();
  }

  _saveCookie(cookie) {
    fs.writeFile("cookie.txt", cookie, (err) => {
      if (err) console.log(err);
    });
  }

  _setObjProps(propName) {
    let thisObj = this;
    thisObj.$(`select[name="${propName}"] > option`).each(async function() {
      if(thisObj.$(this).val()) {
        thisObj[propName].push({
          title: thisObj.$(this).text(),
          value: thisObj.$(this).val()
        });
      }
    });
  }

  get pageToken() {
    return this._pageToken;
  }

  async logout() {
    console.log('>>Logout...');
    await this._http(`${this._baseUrl}/auth/logout`);
  }

  async login() {
    console.log('>>Login process...');
    const homePage = await (await this._http(this._baseUrl)).text();
    this.$ = cheerio.load(homePage);
    this._setPageToken();
    let loginParams = new URLSearchParams();
    loginParams.append('email', this._email);
    loginParams.append('password', this._password);
    loginParams.append('POTPtokenCode', this._pageToken);

    const loginResponse = await (await this._http(`${this._baseUrl}/auth/login`, { method: 'POST', body: loginParams }))
    
    const loginResponseJson = await loginResponse.json();

    if (loginResponseJson.status  === 'ok') {
      const cookie = loginResponse.headers.get('set-cookie');
      this._saveCookie(cookie);

      const dashResponse = await this._http(this._berandaUrl);
      const dashPage = await dashResponse.text();

      this.$ = cheerio.load(dashPage);

      let dashExist = this.$('select[name="jenis_kayu"] > option').text()

      if(dashExist) {
        console.log('Login success');
        let thisObj = this;

        thisObj._propNames.forEach(item => {
          thisObj._setObjProps(item);
        });

        thisObj.$('select[name="wilayah"] > option').each(async function() {
          if(thisObj.$(this).val()) {
            thisObj.wilayah.push({
              title: thisObj.$(this).text(),
              value: thisObj.$(this).val()
            });

            let wilayah_id = thisObj.$(this).val()
            let slctKotaParams = new URLSearchParams();
            slctKotaParams.append('wilayah', wilayah_id);
            slctKotaParams.append('POTPtokenCode', thisObj._pageToken);
            const slctKota = await (await thisObj._http(`${thisObj._berandaUrl}/select_kota`, { method: 'POST', body: slctKotaParams })).text();
            
            thisObj.$(slctKota).find('select[name="kota"] > option').each(async function() {
              if(thisObj.$(this).val()) {
                thisObj.kota.push({
                  reff_id: wilayah_id,
                  title: thisObj.$(this).text(),
                  value: thisObj.$(this).val()
                });
              }

              let kota_id = thisObj.$(this).val();
              let slctTpkParams = new URLSearchParams();
              slctTpkParams.append('kota', kota_id);
              slctTpkParams.append('POTPtokenCode', thisObj._pageToken);
              const slctTpk = await (await thisObj._http(`${thisObj._berandaUrl}/select_tpk`, { method: 'POST', body: slctTpkParams })).text()
            
              thisObj.$(slctTpk).find('select[name="tpk"] > option').each(function() {
                if(thisObj.$(this).val()) {
                  thisObj.tpk.push({
                    reff_id: kota_id,
                    title: thisObj.$(this).text(),
                    value: thisObj.$(this).val()
                  });
                }  
              });
            });
          }
        });

        return { status: true, message: 'Login Success'}
      } else {
        let message = this.$('#home h2').text();
        return { status: false, message: message || 'invalid login'};
      }
    
    } else {
      this.logout();
      if (loginResponseJson.status  === 'error') {
        return { status: false, message: loginResponseJson.message }
      }
      
      return { status: false, message: loginResponse.status }
    }
    
    
  }

  async searching({wilayah, kota, tpk, kayu, mutu, sortimen}) {

    const dataTableUrl = `${this._baseUrl}/filter/jsonProduct/${wilayah}/${kota}/${tpk}/${kayu}/${mutu}/${sortimen}/semua-jenis-tebangan/semua-status/semua-cacat-kayu`;

    const dataTable = await (await this._http(dataTableUrl)).text();

    let sanitizedData = dataTable.replace(/<span style=\\"font-weight: 600\\">/g, '').replace(/<\\\/span>/g, '').replace(/\s+/g, '').replace(/\\n/g, '');

    this.availableOrder = JSON.parse(sanitizedData).data.map((item) => {
      if(item[1] === '0') {
        let newItem = {
          reff_id: item[0],
          title: `${item[2]} | ${item[19].padEnd(7)} | ${item[13]} | ${item[9].padEnd(8)} | Pjg:${item[10].padEnd(9)} | Tbl:${item[12]} | Vol:${item[15]} | ${item[18].padEnd(9)} | ${item[17]} |  Qty:${item[14].padEnd(3)} | Hrg Awl:Rp ${item[3].padEnd(10)} | Hrg Akhr:Rp ${item[6].padEnd(10)}`,
          value: item[0]
        };

        if (newItem) return newItem;
      }
      
    });

    return this.availableOrder;

  }

  async order(listOrder) {
    let orderParams = new URLSearchParams();
    orderParams.append('POTPtokenCode', this._pageToken);

    listOrder.forEach(kaplingId => {
      orderParams.append('select_all', 1);
      orderParams.append('id[]', kaplingId);
    });

    const orderResponse = await (await this._http(`${this._baseUrl}/filter/valSubmit`, { method: 'POST', body: orderParams }));
    
    const orderResponseJson = await orderResponse.text();

    console.log(orderResponseJson);

  }

  async checkOrder() {
    let url = 'https://www.tokoperhutani.com/member/jsonOrderToday';
  }

  async cancelOrder()  {
    let url = `https://www.tokoperhutani.com/filter/deleteTrolley`;
  }
}

module.exports = function(email, password) {
  return new Bot(email, password);
};